package com.e.theaauto.activities.map;

import android.annotation.SuppressLint;

import com.e.theaauto.core.BasePresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;
import com.e.theaauto.utils.Constants;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MapActivityPresenter extends BasePresenter<IMapActivity> implements IMapActivityPresenter {

    private NetworkService networkService;
    private String token;
    private final CompositeDisposable disposables;


    public MapActivityPresenter(IMapActivity view, DataModel dataModel, NetworkService networkService) {
        super(view);
        this.networkService = networkService;
        this.token = dataModel.getData(Constants.TAG_TOKEN);
        disposables = new CompositeDisposable();
    }

    @Override
    public void onError(String msg) {
        view.showToast(msg);
        view.dismissProgressDialog();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCarState(String imei) {
        disposables.add(
        Observable.interval(5, TimeUnit.SECONDS)
                .flatMap(aLong -> networkService.getCarsStates(Constants.TOKEN_PREFIX + token))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(carStates -> {
                    for (int i = 0; i < carStates.size(); i++){
                        if (carStates.get(i).imei.equals(imei)){
                            view.updateCarState(carStates.get(i));
                        }
                    }
                    }, throwable -> onError(throwable.getMessage()))
        );
    }

    @SuppressLint("CheckResult")
    @Override
    public void getTrip(String start) {
        networkService.getTrips(Constants.TOKEN_PREFIX + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trips -> {
                    for (int i = 0; i < trips.size(); i++) {
                        if (start.equals(trips.get(i).start)) {
                            view.updateTrip(trips.get(i));
                        }
                    }
                }, throwable -> onError(throwable.getMessage()));
    }

    @Override
    public void onDestroy() {
        disposables.clear();
    }
}
