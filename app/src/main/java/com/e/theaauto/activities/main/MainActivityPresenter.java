package com.e.theaauto.activities.main;


import android.annotation.SuppressLint;
import android.util.Log;

import com.e.theaauto.activities.singIn.SingInActivity;
import com.e.theaauto.core.BasePresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;
import com.e.theaauto.utils.Constants;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivityPresenter extends BasePresenter<IMainActivity> implements IMainActivityPresenter {

    private DataModel dataModel;
    private NetworkService networkService;
    private String token;

    public MainActivityPresenter(IMainActivity view, DataModel dataModel, NetworkService networkService) {
        super(view);
        this.dataModel = dataModel;
        this.networkService = networkService;
        token = dataModel.getData(Constants.TAG_TOKEN);
    }

    @SuppressLint("CheckResult")
    @Override
    public void logout() {
        networkService.logout(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            dataModel.clearData();
                            view.startActivity(SingInActivity.class);
                        }, throwable -> onError(throwable.getMessage()));
    }

    @Override
    public void onDestroy() {
        if (!dataModel.getBoolean(Constants.TAG_SAVE_USER)){
            dataModel.clearData();
        }
    }

    @Override
    public void onError(String msg){
        Log.d(Constants.TAG_API, msg);
        view.showToast(msg);
        view.dismissProgressDialog();
    }
}

