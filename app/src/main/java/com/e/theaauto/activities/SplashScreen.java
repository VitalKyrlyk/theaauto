package com.e.theaauto.activities;

import android.content.Intent;
import android.os.Bundle;

import com.e.theaauto.activities.main.MainActivity;
import com.e.theaauto.activities.singIn.SingInActivity;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.utils.Constants;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class SplashScreen extends DaggerAppCompatActivity {

    @Inject
    DataModel dataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean logIn =  dataModel.getBoolean(Constants.TAG_SAVE_USER);
        Intent intent = new Intent();

        if (logIn){
            intent.setClass(this, MainActivity.class);
        } else {
            intent.setClass(this, SingInActivity.class);
        }

        startActivity(intent);
        finish();

    }
}
