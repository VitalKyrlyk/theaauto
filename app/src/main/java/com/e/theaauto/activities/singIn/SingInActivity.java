package com.e.theaauto.activities.singIn;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.e.theaauto.R;
import com.e.theaauto.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class SingInActivity extends DaggerAppCompatActivity implements ISingInActivity {

    @Inject
    ISingInActivityPresenter presenter;

    ProgressDialog progressDialog;

    String email, password;

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPass;
    @BindView(R.id.switch_save_user)
    Switch switchSaveUser;
    @BindView(R.id.base_layout)
    RelativeLayout baseLayout;

    @OnClick(R.id.tv_log_in_automatically)
    void clickTvAutoLogIn(){
        switchSaveUser.setChecked(!switchSaveUser.isChecked());
    }

    @BindView(R.id.btn_sign_in)
    Button btnSingIn;
    @OnClick(R.id.btn_sign_in)
    void clickSingIn(){
        btnSingIn.setEnabled(false);
        new Handler().postDelayed(() -> btnSingIn.setEnabled(true), 500);
        progressDialog.setMessage(Constants.TITLE_PROGRES_DIALOG);

        email = etEmail.getText().toString();
        password = etPass.getText().toString();

        if (validate()){
            btnSingIn.setEnabled(false);
            presenter.login(email, password, switchSaveUser.isChecked());
            progressDialog.show();
        }
    }

    @BindView(R.id.btn_sign_up)
    Button btnSingUp;
    @OnClick(R.id.btn_sign_up)
    void clickSingUp(){
        btnSingUp.setEnabled(false);
        new Handler().postDelayed(() -> btnSingUp.setEnabled(true), 500);
//        startActivity(new Intent(this, SingUpActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);
        ButterKnife.bind(this);
        setUpProgressDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    private boolean validate(){
        boolean validate = true;

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches()) {
            etEmail.setError(getResources().getString(R.string.wrong_email));
            validate = false;
        }
        if (password.isEmpty() || password.length() < 6){
            etPass.setError(getResources().getString(R.string.wrong_password));
            validate = false;
        }
        return validate;
    }

    @Override
    public void setUpProgressDialog(){
        progressDialog = new ProgressDialog(SingInActivity.this,
                R.style.AppTheme_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void dismissProgressDialog() {
        progressDialog.dismiss();
        btnSingIn.setEnabled(true);
    }
}
