package com.e.theaauto.activities.map;

import com.e.theaauto.core.baseActivity.IBaseViewActivity;
import com.e.theaauto.network.enteties.CarState;
import com.e.theaauto.network.enteties.Trip;

public interface IMapActivity extends IBaseViewActivity {

    void updateCarState(CarState carState);

    void updateTrip(Trip trip);

}
