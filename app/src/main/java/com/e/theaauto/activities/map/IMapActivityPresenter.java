package com.e.theaauto.activities.map;

import com.e.theaauto.core.baseActivity.IBasePresenterActivity;

public interface IMapActivityPresenter extends IBasePresenterActivity<IMapActivity> {

    void getCarState(String imei);

    void getTrip(String start);

    void onDestroy();

}
