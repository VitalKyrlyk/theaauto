package com.e.theaauto.activities.singIn;

import com.e.theaauto.core.baseActivity.IBasePresenterActivity;

public interface ISingInActivityPresenter extends IBasePresenterActivity<ISingInActivity> {

    void login(String email, String password, boolean saveUser);

}
