package com.e.theaauto.activities.map;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.e.theaauto.R;
import com.e.theaauto.adapters.marker.MarkerInfoWindowAdapter;
import com.e.theaauto.network.enteties.CarState;
import com.e.theaauto.network.enteties.Trip;
import com.e.theaauto.utils.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class MapActivity extends DaggerAppCompatActivity implements IMapActivity, OnMapReadyCallback {

    private GoogleMap map;
    private ProgressDialog progressDialog;

    @Inject
    IMapActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        setUpProgressDialog();
        initMap();

        String fragment = Objects.requireNonNull(getIntent().getExtras()).getString(Constants.TAG_FRAGMENT);
        if (fragment != null) {
            if (fragment.equals(Constants.TAG_CARS_FRAGMENT)) {
                String imei = getIntent().getExtras().getString(Constants.TAG_ID);
                presenter.getCarState(imei);
            } else if (fragment.equals(Constants.TAG_TRIPS_FRAGMENT)) {
                String start = getIntent().getExtras().getString(Constants.TAG_TIME_START);
                presenter.getTrip(start);
            }
            progressDialog.show();
        }
    }

    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        MarkerInfoWindowAdapter markerInfoWindowAdapter = new MarkerInfoWindowAdapter(getApplicationContext());
        map.setInfoWindowAdapter(markerInfoWindowAdapter);
    }

    @Override
    public void startActivity(Class activity) {
        startActivity(new Intent(this, activity));
    }

    @Override
    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void setUpProgressDialog(){
        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(Constants.TITLE_PROGRES_DIALOG);
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateCarState(CarState carState) {
        LatLng carLocation = new LatLng(carState.latitude, carState.longitude);
        if (map != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(carLocation, 16.0f));
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(carLocation)
                    .draggable(true));
            marker.setTag(carState);
            marker.showInfoWindow();
        }
        progressDialog.dismiss();
    }

    @Override
    public void updateTrip(Trip trip) {
        List<LatLng> lngs = new ArrayList<>();
        for (int i = 0; i < trip.points.size(); i++){
            LatLng latLng = new LatLng(trip.points.get(i).geo.get(0), trip.points.get(i).geo.get(1));
            lngs.add(latLng);
        }
        LatLng end = new LatLng(trip.points.get(0).geo.get(0).doubleValue(), trip.points.get(0).geo.get(1).doubleValue());
        LatLng start = new LatLng(trip.points.get(trip.points.size() - 1).geo.get(0), trip.points.get(trip.points.size()-1).geo.get(1));
        if (map != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(end, 16.0f));
            PolylineOptions opts = new PolylineOptions().addAll(lngs).color(Color.BLUE).width(10);
            map.addPolyline(opts);
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(end)
                    .title("End")
                    .draggable(true));
            marker.setTag(trip);
            map.addMarker(new MarkerOptions()
                    .position(start)
                    .title("Start")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
                    .draggable(true)).setTag(trip);
            marker.showInfoWindow();
        }
        progressDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
