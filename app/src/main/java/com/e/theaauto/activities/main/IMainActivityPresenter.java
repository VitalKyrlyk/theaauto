package com.e.theaauto.activities.main;

import com.e.theaauto.core.baseActivity.IBasePresenterActivity;

public interface IMainActivityPresenter extends IBasePresenterActivity<IMainActivity> {

    void logout();

    void onDestroy();

}
