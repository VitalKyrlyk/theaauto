package com.e.theaauto.activities.singIn;

import android.annotation.SuppressLint;
import android.util.Log;

import com.e.theaauto.activities.main.MainActivity;
import com.e.theaauto.core.BasePresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;
import com.e.theaauto.network.enteties.SingInRequest;
import com.e.theaauto.utils.Constants;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SingInActivityPresenter extends BasePresenter<ISingInActivity> implements ISingInActivityPresenter {

    private DataModel dataModel;
    private NetworkService networkService;

    public SingInActivityPresenter(ISingInActivity view, DataModel dataModel, NetworkService networkService) {
        super(view);
        this.dataModel = dataModel;
        this.networkService = networkService;
    }

    @SuppressLint("CheckResult")
    @Override
    public void login(String email, String password, boolean saveUser) {
        networkService.singIn(new SingInRequest(email, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> onSuccessToken(response.getToken(), saveUser), throwable -> onError(throwable.getMessage()));
    }

    private void onSuccessToken(String token, boolean saveUser){
        dataModel.putData(Constants.TAG_TOKEN, token);
        dataModel.putBoolean(Constants.TAG_SAVE_USER, saveUser);
        view.dismissProgressDialog();
        view.startActivity(MainActivity.class);
    }

    @Override
    public void onError(String msg){
        Log.d(Constants.TAG_API, msg);
        view.showToast(msg);
        view.dismissProgressDialog();
    }
}
