package com.e.theaauto.fragments.main;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.e.theaauto.R;
import com.e.theaauto.activities.map.MapActivity;
import com.e.theaauto.adapters.recyclerView.Adapter;
import com.e.theaauto.adapters.recyclerView.AdapterDH;
import com.e.theaauto.core.adapter.OnItemClickListener;
import com.e.theaauto.core.baseFragment.BaseFragment;
import com.e.theaauto.core.paddingDecoration.PaddingDecoration;
import com.e.theaauto.network.enteties.Car;
import com.e.theaauto.network.enteties.Trip;
import com.e.theaauto.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends BaseFragment implements IMainFragment, OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    IMainFragmentPresenter presenter;

    private View view;
    private Adapter adapter;
    private ProgressDialog progressDialog;
    private String TAG_FRAGMENT;

    @BindView(R.id.rv_cars)
    RecyclerView recyclerView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    public MainFragment(String TAG_FRAGMENT) {
        this.TAG_FRAGMENT = TAG_FRAGMENT;
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment, null);
        ButterKnife.bind(this, view);

        setUpAdapter();
        setUpProgressDialog();
        getData();

        return view;
    }

    private void getData(){
        if (!TAG_FRAGMENT.isEmpty()) {
            if (TAG_FRAGMENT.equals(Constants.TAG_TRIPS_FRAGMENT)) {
                presenter.getTrips();
            } else {
                presenter.getCars();
            }
            progressDialog.show();
        }
    }

    @Override
    public void setUpProgressDialog(){
        progressDialog = new ProgressDialog(view.getContext(),
                R.style.AppTheme_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(Constants.TITLE_PROGRES_DIALOG);
    }

    @Override
    public void updateCarsList(List<Car> cars) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                dismissProgressDialog();
                ArrayList<AdapterDH> carDHS = new ArrayList<>();
                for (Car car : cars) {
                    carDHS.add(new AdapterDH(car, Adapter.CAR_TYPE));
                }
                adapter.setListDH(carDHS);
            });
        }
    }

    @Override
    public void updateTripsList(List<Trip> trips) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                dismissProgressDialog();
                ArrayList<AdapterDH> carDHS = new ArrayList<>();
                for (Trip trip : trips) {
                    carDHS.add(new AdapterDH(trip, Adapter.TRIP_TYPE));
                }
                adapter.setListDH(carDHS);
            });
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);
        progressDialog.dismiss();
    }

    @Override
    public void setUpAdapter() {
        swipeContainer.setColorSchemeColors(view.getResources().getColor(R.color.colorAccent));
        swipeContainer.setOnRefreshListener(this);
        adapter = new Adapter();
        LinearLayoutManager manager = new LinearLayoutManager(view.getContext());
        adapter.setOnItemClickListener(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new PaddingDecoration(view.getContext(), 15));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(view.getContext(), toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view, int position, int viewType, Object object) {
        if (viewType == Adapter.CAR_TYPE){
            if (object instanceof Car){
                Intent intent = new Intent(view.getContext(), MapActivity.class);
                intent.putExtra(Constants.TAG_ID, ((Car) object).imei);
                intent.putExtra(Constants.TAG_FRAGMENT, Constants.TAG_CARS_FRAGMENT);
                startActivity(intent);
                view.setEnabled(false);
                new Handler().postDelayed(() -> view.setEnabled(true), 500);
            }
        } else if (viewType == Adapter.TRIP_TYPE){
            if (object instanceof Trip){
                Intent intent = new Intent(view.getContext(), MapActivity.class);
                intent.putExtra(Constants.TAG_FRAGMENT, Constants.TAG_TRIPS_FRAGMENT);
                intent.putExtra(Constants.TAG_TIME_START, ((Trip) object).start);
                startActivity(intent);
                view.setEnabled(false);
                new Handler().postDelayed(() -> view.setEnabled(true), 500);
            }
        }
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
