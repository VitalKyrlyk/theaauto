package com.e.theaauto.fragments.main;

import android.annotation.SuppressLint;
import android.util.Log;

import com.e.theaauto.core.BasePresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;
import com.e.theaauto.utils.Constants;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainFragmentPresenter extends BasePresenter<IMainFragment> implements IMainFragmentPresenter {

    private NetworkService networkService;
    private String token;

    public MainFragmentPresenter(IMainFragment view, DataModel dataModel, NetworkService networkService) {
        super(view);
        this.networkService = networkService;
        token = dataModel.getData(Constants.TAG_TOKEN);
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCars(){
        networkService.getCars(Constants.TOKEN_PREFIX + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> view.updateCarsList(response), throwable -> onError(throwable.getMessage()));
    }

    @SuppressLint("CheckResult")
    @Override
    public void getTrips() {
        networkService.getTrips(Constants.TOKEN_PREFIX + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> view.updateTripsList(response), throwable -> onError(throwable.getMessage()));
    }

    @Override
    public void onError(String msg) {
        Log.d(Constants.TAG_API, msg);
        view.showToast(msg);
        view.dismissProgressDialog();
    }
}
