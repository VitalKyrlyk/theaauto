package com.e.theaauto.fragments.main;

import com.e.theaauto.core.baseFragment.IBaseViewFragment;
import com.e.theaauto.network.enteties.Car;
import com.e.theaauto.network.enteties.Trip;

import java.util.List;

public interface IMainFragment extends IBaseViewFragment {

    void updateCarsList(List<Car> cars);

    void updateTripsList(List<Trip> cars);

    void dismissProgressDialog();

}
