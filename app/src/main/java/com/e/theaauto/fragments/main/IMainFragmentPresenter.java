package com.e.theaauto.fragments.main;

import com.e.theaauto.core.baseFragment.IBasePresenterFragment;

public interface IMainFragmentPresenter extends IBasePresenterFragment<IMainFragment> {

    void getCars();

    void getTrips();

}
