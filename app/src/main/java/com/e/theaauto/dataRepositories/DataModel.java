package com.e.theaauto.dataRepositories;

import android.content.SharedPreferences;
import android.util.Log;

import com.e.theaauto.utils.Constants;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataModel {

    private SharedPreferences sharedPreferences;

    @Inject
    DataModel(SharedPreferences sharedPreferences){
        this.sharedPreferences = sharedPreferences;
    }

    public void putData(String key, String data){
        sharedPreferences.edit().putString(key, data).apply();
        Log.d(Constants.TAG_DATA, "putData: " + key + ": " + data);
    }

    public String getData(String key){
        return sharedPreferences.getString(key, "");
    }

    public void putBoolean(String key, boolean b){
        sharedPreferences.edit().putBoolean(key, b).apply();
        Log.d(Constants.TAG_DATA, "putBoolean: " + key + ": " + b);
    }

    public boolean getBoolean(String key){
        return sharedPreferences.getBoolean(key, false);
    }

    public boolean clearData(){
        sharedPreferences.edit().clear().apply();
        Log.d(Constants.TAG_DATA, "clearData");
        return true;
    }
}
