package com.e.theaauto.core.adapter;

import android.view.View;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.RecyclerView;

public abstract class RecyclerVH<DH extends RecyclerDH> extends RecyclerView.ViewHolder {

    public RecyclerVH(View itemView) {
        super(itemView);
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T findView(@IdRes int viewId) {
        return (T) itemView.findViewById(viewId);
    }

    public abstract void setListeners(final OnItemClickListener listener);

    public abstract void bindData(DH data);

}
