package com.e.theaauto.core.baseFragment;



import com.e.theaauto.core.IBaseView;


public interface IBaseViewFragment extends IBaseView {

    void setUpAdapter();

}
