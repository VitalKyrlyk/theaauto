package com.e.theaauto.core;

public interface IBasePresenter<V> {

    void onError(String msg);

}
