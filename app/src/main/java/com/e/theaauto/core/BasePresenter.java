package com.e.theaauto.core;

public abstract class BasePresenter<V> implements IBasePresenter<V> {

    public V view;

    public BasePresenter(V view){
        this.view = view;
    }

}
