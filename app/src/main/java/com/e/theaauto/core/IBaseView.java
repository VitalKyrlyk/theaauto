package com.e.theaauto.core;

public interface IBaseView {

    void showToast(String toast);

    void setUpProgressDialog();

}
