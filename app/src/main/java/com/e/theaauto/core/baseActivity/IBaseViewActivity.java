package com.e.theaauto.core.baseActivity;


import com.e.theaauto.core.IBaseView;

public interface IBaseViewActivity extends IBaseView {

    void startActivity(Class activity);

    void dismissProgressDialog();

}
