package com.e.theaauto.dagger.module.activities.map;

import com.e.theaauto.activities.map.IMapActivity;
import com.e.theaauto.activities.map.IMapActivityPresenter;
import com.e.theaauto.activities.map.MapActivityPresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;

import dagger.Module;
import dagger.Provides;

@Module
public class MapModule {

    @Provides
    IMapActivityPresenter provideIMapActivityPresenter(IMapActivity view, DataModel dataModel, NetworkService networkService){
        return new MapActivityPresenter(view, dataModel, networkService);
    }

}
