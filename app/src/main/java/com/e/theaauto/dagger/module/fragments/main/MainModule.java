package com.e.theaauto.dagger.module.fragments.main;

import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.fragments.main.MainFragmentPresenter;
import com.e.theaauto.fragments.main.IMainFragment;
import com.e.theaauto.fragments.main.IMainFragmentPresenter;
import com.e.theaauto.network.NetworkService;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    @Provides
    IMainFragmentPresenter provideIMainFragmentPresenter(IMainFragment view, DataModel dataModel, NetworkService networkService){
        return new MainFragmentPresenter(view, dataModel, networkService);
    }

}
