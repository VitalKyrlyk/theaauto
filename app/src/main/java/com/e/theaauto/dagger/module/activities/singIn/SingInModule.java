package com.e.theaauto.dagger.module.activities.singIn;

import com.e.theaauto.activities.singIn.ISingInActivity;
import com.e.theaauto.activities.singIn.ISingInActivityPresenter;
import com.e.theaauto.activities.singIn.SingInActivityPresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;

import dagger.Module;
import dagger.Provides;

@Module
public class SingInModule {

    @Provides
    ISingInActivityPresenter provideISingInActivityPresenter(ISingInActivity view, DataModel dataModel,  NetworkService networkService){
        return new SingInActivityPresenter(view, dataModel, networkService);
    }

}
