package com.e.theaauto.dagger.module.activities.main;

import com.e.theaauto.activities.main.IMainActivity;
import com.e.theaauto.activities.main.MainActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainViewModule {

    @Binds
    abstract IMainActivity provideIMainActivity(MainActivity mainActivity);

}
