package com.e.theaauto.dagger;

import com.e.theaauto.activities.SplashScreen;
import com.e.theaauto.activities.main.MainActivity;
import com.e.theaauto.activities.map.MapActivity;
import com.e.theaauto.activities.singIn.SingInActivity;
import com.e.theaauto.dagger.module.activities.main.MainModule;
import com.e.theaauto.dagger.module.activities.main.MainViewModule;
import com.e.theaauto.dagger.module.activities.map.MapModule;
import com.e.theaauto.dagger.module.activities.map.MapViewModule;
import com.e.theaauto.dagger.module.activities.singIn.SingInModule;
import com.e.theaauto.dagger.module.activities.singIn.SingInViewModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module()
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract SplashScreen bindSplashScreen();

    @ContributesAndroidInjector(modules = {MainModule.class, MainViewModule.class})
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {SingInModule.class, SingInViewModule.class})
    abstract SingInActivity bindSingInActivity();

    @ContributesAndroidInjector(modules = {MapModule.class, MapViewModule.class})
    abstract MapActivity bindMapActivity();

}
