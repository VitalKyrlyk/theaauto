package com.e.theaauto.dagger.module.activities.singIn;

import com.e.theaauto.activities.singIn.ISingInActivity;
import com.e.theaauto.activities.singIn.SingInActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SingInViewModule {

    @Binds
    abstract ISingInActivity provideISingInActivity(SingInActivity singInActivity);

}
