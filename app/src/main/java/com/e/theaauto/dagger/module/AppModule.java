package com.e.theaauto.dagger.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.e.theaauto.App;
import com.e.theaauto.utils.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(App application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences (Context context){
        return context.getSharedPreferences(Constants.TAG_LOGIN, Context.MODE_PRIVATE);
    }

}
