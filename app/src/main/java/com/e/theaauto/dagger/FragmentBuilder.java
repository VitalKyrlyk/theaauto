package com.e.theaauto.dagger;

import com.e.theaauto.dagger.module.fragments.main.MainModule;
import com.e.theaauto.dagger.module.fragments.main.MainViewModule;
import com.e.theaauto.fragments.main.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module()
public abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = {MainModule.class, MainViewModule.class})
    abstract MainFragment bindMainFragment();

}
