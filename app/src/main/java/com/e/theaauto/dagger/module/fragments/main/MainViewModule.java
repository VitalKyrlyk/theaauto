package com.e.theaauto.dagger.module.fragments.main;

import com.e.theaauto.fragments.main.MainFragment;
import com.e.theaauto.fragments.main.IMainFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainViewModule {

    @Binds
    abstract IMainFragment provideIMainFragment(MainFragment carsFragment);

}
