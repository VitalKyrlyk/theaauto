package com.e.theaauto.dagger.module.activities.map;

import com.e.theaauto.activities.map.IMapActivity;
import com.e.theaauto.activities.map.MapActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MapViewModule {

    @Binds
    abstract IMapActivity provideIMapActivity(MapActivity mapActivity);

}
