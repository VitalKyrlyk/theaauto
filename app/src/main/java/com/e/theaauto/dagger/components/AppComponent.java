package com.e.theaauto.dagger.components;

import com.e.theaauto.App;
import com.e.theaauto.dagger.ActivityBuilder;
import com.e.theaauto.dagger.FragmentBuilder;
import com.e.theaauto.dagger.module.AppModule;
import com.e.theaauto.dagger.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        ActivityBuilder.class,
        FragmentBuilder.class})

public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {

    }

}