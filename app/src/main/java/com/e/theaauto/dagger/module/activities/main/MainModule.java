package com.e.theaauto.dagger.module.activities.main;

import com.e.theaauto.activities.main.IMainActivity;
import com.e.theaauto.activities.main.IMainActivityPresenter;
import com.e.theaauto.activities.main.MainActivityPresenter;
import com.e.theaauto.dataRepositories.DataModel;
import com.e.theaauto.network.NetworkService;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    @Provides
    IMainActivityPresenter provideIMainActivityPresenter(IMainActivity view, DataModel dataModel, NetworkService networkService){
        return new MainActivityPresenter(view, dataModel, networkService);
    }

}
