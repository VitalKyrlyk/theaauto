package com.e.theaauto.adapters.recyclerView;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.e.theaauto.R;
import com.e.theaauto.core.adapter.OnItemClickListener;
import com.e.theaauto.core.adapter.RecyclerVH;
import com.e.theaauto.network.enteties.Car;

public class CarVH extends RecyclerVH<AdapterDH> {

    private View view;
    private TextView tvCarName, tvCarStatus, tvCarModel, tvCarImei;

    private OnItemClickListener listener;

    CarVH(View itemView) {
        super(itemView);
        this.view = itemView;
        this.tvCarName = itemView.findViewById(R.id.tv_car_name);
        this.tvCarModel = itemView.findViewById(R.id.tv_car_model);
        this.tvCarStatus = itemView.findViewById(R.id.tv_car_status);
        this.tvCarImei = itemView.findViewById(R.id.tv_car_imei);

    }

    @Override
    public void setListeners(OnItemClickListener listener) {
        this.listener = listener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void bindData(AdapterDH data) {

        Car car = data.getCar();
        tvCarName.setText(car.nickname);
        tvCarModel.setText(car.model);
        tvCarStatus.setText(car.status);
        tvCarImei.setText(view.getResources().getString(R.string.imei) + car.imei);
        view.setOnClickListener(view ->
                listener.onClick(view, getAdapterPosition(), getItemViewType(), car));

    }
}
