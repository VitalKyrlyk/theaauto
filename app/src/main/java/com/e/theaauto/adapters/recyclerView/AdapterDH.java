package com.e.theaauto.adapters.recyclerView;

import com.e.theaauto.core.adapter.RecyclerDH;
import com.e.theaauto.network.enteties.Car;
import com.e.theaauto.network.enteties.Trip;

public class AdapterDH implements RecyclerDH {

    private Car car;
    private Trip trip;
    private int type;

    public AdapterDH(Car car, int type) {
        this.car = car;
        this.type = type;
    }

    public AdapterDH(Trip trip, int type) {
        this.trip = trip;
        this.type = type;
    }

    Car getCar() {
        return car;
    }

    Trip getTrip(){
        return trip;
    }

    int getGroupType() {
        return type;
    }
}
