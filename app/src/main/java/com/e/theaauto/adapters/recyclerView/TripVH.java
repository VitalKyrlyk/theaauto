package com.e.theaauto.adapters.recyclerView;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.e.theaauto.R;
import com.e.theaauto.core.adapter.OnItemClickListener;
import com.e.theaauto.core.adapter.RecyclerVH;
import com.e.theaauto.network.enteties.Trip;
import com.e.theaauto.utils.Constants;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class TripVH extends RecyclerVH<AdapterDH> {

    private View view;
    private TextView tvTripDistance, tvTripStart, tvTripStatus, tvTripEnd, tvTripTime;
    private OnItemClickListener listener;

    TripVH(View itemView) {
        super(itemView);
        this.view = itemView;
        this.tvTripDistance = itemView.findViewById(R.id.tv_trip_distance);
        this.tvTripTime = itemView.findViewById(R.id.tv_trip_time);
        this.tvTripStart = itemView.findViewById(R.id.tv_trip_start);
        this.tvTripEnd = itemView.findViewById(R.id.tv_trip_end);
        this.tvTripStatus = itemView.findViewById(R.id.tv_trip_status);
    }

    @Override
    public void setListeners(OnItemClickListener listener) {
        this.listener = listener;
    }

    @SuppressLint("SetTextI18n, SimpleDateFormat")
    @Override
    public void bindData(AdapterDH data) {
        Trip trip = data.getTrip();

        tvTripDistance.setText(trip.distance / 1000 + "km");
        tvTripStart.setText(view.getResources().getString(R.string.start)
                + trip.start);
        DateFormat format = new SimpleDateFormat("HH:mm:ss") ;
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String drivingTime = format.format(new Date(trip.drivingTime*1000));
        tvTripTime.setText(drivingTime);
        tvTripEnd.setText(view.getResources().getString(R.string.end)
                + trip.end);
        if (trip.finished) {
            tvTripStatus.setText(view.getResources().getString(R.string.finished));
        } else {
            tvTripStatus.setText(view.getResources().getString(R.string.active));
        }
        view.setOnClickListener(view ->
                listener.onClick(view, getAdapterPosition(), getItemViewType(), trip));

    }
}
