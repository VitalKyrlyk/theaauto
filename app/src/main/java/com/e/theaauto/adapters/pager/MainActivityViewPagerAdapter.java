package com.e.theaauto.adapters.pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.e.theaauto.fragments.main.MainFragment;
import com.e.theaauto.utils.Constants;

public class MainActivityViewPagerAdapter extends FragmentPagerAdapter{

    public enum MainActivityPages {
        TAB_CARS(0, Constants.CARS_FRAGMENT, new MainFragment(Constants.TAG_CARS_FRAGMENT)),
        TAB_TRIPS(1, Constants.TRIPS_FRAGMENT, new MainFragment(Constants.TAG_TRIPS_FRAGMENT));

        public int index;
        public String title;
        public Fragment fragment;

        MainActivityPages(int index, String title, Fragment fragment) {
            this.index = index;
            this.title = title;
            this.fragment = fragment;
        }
    }

    public MainActivityViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return MainActivityPages.values()[position].fragment;
    }

    @Override
    public int getCount() {
        return MainActivityPages.values().length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return MainActivityPages.values()[position].title;
    }

}