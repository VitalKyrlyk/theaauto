package com.e.theaauto.adapters.marker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.e.theaauto.R;
import com.e.theaauto.network.enteties.CarState;
import com.e.theaauto.network.enteties.Trip;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public MarkerInfoWindowAdapter(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public View getInfoContents(Marker marker) {
        View view;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (marker.getTag() instanceof CarState){
            CarState carState = (CarState) marker.getTag();
            view = inflater.inflate(R.layout.map_marker_info_window_car, null);
            TextView tvCarState = view.findViewById(R.id.tv_car_state);
            TextView tvCarSpeed = view.findViewById(R.id.tv_car_speed);
            TextView tvCarLatitude = view.findViewById(R.id.tv_car_latitude);
            TextView tvCarLongitude = view.findViewById(R.id.tv_car_longitude);
            TextView tvCarTemperatureOutside = view.findViewById(R.id.tv_car_temperatureOutside);
            TextView tvCarTemperatureInside = view.findViewById(R.id.tv_car_temperatureInside);

            tvCarState.setText(view.getResources().getString(R.string.state) + carState.state);
            tvCarSpeed.setText(view.getResources().getString(R.string.speed) + carState.speed);
            tvCarLatitude.setText(view.getResources().getString(R.string.latitude) + carState.latitude);
            tvCarLongitude.setText(view.getResources().getString(R.string.longitude) + carState.longitude);
            tvCarTemperatureOutside.setText(view.getResources().getString(R.string.temperatureOutside) + carState.temperatureOutside);
            tvCarTemperatureInside.setText(view.getResources().getString(R.string.temperatureInside) + carState.temperatureInside);
        } else if (marker.getTag() instanceof Trip){
            Trip trip = (Trip) marker.getTag();
            view = inflater.inflate(R.layout.map_marker_info_window_trip, null);
            TextView tv_distance = view.findViewById(R.id.tv_distance);
            TextView tv_driving_time = view.findViewById(R.id.tv_driving_time);
            TextView tv_start = view.findViewById(R.id.tv_start);
            TextView tv_end = view.findViewById(R.id.tv_end);

            tv_distance.setText(view.getResources().getString(R.string.distance) + trip.distance / 1000 + "km");
            @SuppressLint("SimpleDateFormat")
            DateFormat format = new SimpleDateFormat("HH:mm:ss") ;
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            String drivingTime = format.format(new Date(trip.drivingTime*1000));

            tv_driving_time.setText(view.getResources().getString(R.string.duration) + drivingTime);
            tv_start.setText(view.getResources().getString(R.string.start) + trip.start);
            tv_end.setText(view.getResources().getString(R.string.end) + trip.end);
        } else {
            throw new RuntimeException("Adapter :: createVH [Can find such view type]");
        }
        return view;
    }
}