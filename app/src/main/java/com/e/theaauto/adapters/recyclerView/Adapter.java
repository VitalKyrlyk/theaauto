package com.e.theaauto.adapters.recyclerView;

import android.view.View;

import androidx.annotation.NonNull;

import com.e.theaauto.R;
import com.e.theaauto.core.adapter.RecyclerAdapter;
import com.e.theaauto.core.adapter.RecyclerVH;

public class Adapter extends RecyclerAdapter<AdapterDH> {

    public static final int CAR_TYPE = 0;
    public static final int TRIP_TYPE = 1;

    @NonNull
    @Override
    public RecyclerVH<AdapterDH> createVH(View view, int viewType) {
        switch (viewType) {
            case CAR_TYPE:
                return new CarVH(view);

            case TRIP_TYPE:
                return new TripVH(view);

            default:
                throw new RuntimeException("Adapter :: createVH [Can find such view type]");
        }
    }

    @Override
    public int getLayoutRes(int viewType) {
        switch (viewType) {
            case CAR_TYPE:
                return R.layout.item_car;

            case TRIP_TYPE:
                return R.layout.item_trip;

            default:
                throw new RuntimeException("Adapter :: getLayoutRes [Can find such view type]");
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getGroupType();
    }

}
