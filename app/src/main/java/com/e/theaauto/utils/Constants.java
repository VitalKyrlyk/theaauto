package com.e.theaauto.utils;

public class Constants {

    public static final String BASE_URL = "https://devapi.mobiliuz.com";
    public static final String TAG_API = "apiTag";
    public static final String TAG_DATA = "dataTag";
    public static final String TAG_LOGIN = "login";
    public static final String TAG_ID = "id";
    public static final String TAG_TIME_START = "time_start";
    public static final String TOKEN_PREFIX = "Token ";
    public static final String TITLE_PROGRES_DIALOG = "Authentication... ";
    public static final String TAG_CARS_FRAGMENT = "cars_fragment";
    public static final String TAG_FRAGMENT = "fragment";
    public static final String CARS_FRAGMENT = "Cars";
    public static final String TAG_TRIPS_FRAGMENT = "trips_fragment";
    public static final String TRIPS_FRAGMENT = "Trips";
    public static final String TAG_TOKEN = "token";
    public static final String TAG_SAVE_USER = "save_user";

}
