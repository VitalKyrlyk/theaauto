package com.e.theaauto;

import com.e.theaauto.dagger.components.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {

    public static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        this.instance = instance;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }
}
