package com.e.theaauto.kotlin

import com.e.theaauto.App
import com.e.theaauto.kotlin.DataUtil.clearPreference


fun appContext() = App.instance

fun test1(){
    networkService
}

fun test2(){
    val token2 = DataUtil.token
    DataUtil.token = "123"
    val isFirstLunch = DataUtil.isFirstLaunch
    DataUtil.isFirstLaunch = false
}

fun test3(){
    clearPreference()
}
