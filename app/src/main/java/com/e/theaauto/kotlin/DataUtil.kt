package com.e.theaauto.kotlin

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.e.theaauto.utils.Constants

object DataUtil {

    var user: String?
        set(value) = putData("user", value)
        get() = appContext().sharedPreference().getString("user", "")
    var token: String?
        set(value) = putData("token", value)
        get() = appContext().sharedPreference().getString("token", "")
    var isFirstLaunch: Boolean
        set(value) = putData("firstLaunch", value)
        get() = appContext().sharedPreference().getBoolean("firstLaunch", true)

    fun clearPreference(){
        appContext().sharedPreference().edit().clear().apply()
    }

    fun <T: Any?>putData(key: String, data: T?){
        appContext().sharedPreference().putData(key, data)
    }

    private fun <T: Any?>SharedPreferences.putData(key: String, data: T?){
        with(edit()){
            when (data) {
                is String? -> putString(key, data).apply()
                is Boolean -> putBoolean(key, data).apply()
                is Int -> putInt(key, data).apply()
                else -> Log.d("SharedPreferences","Not applicable")
            }
        }
        Log.d(Constants.TAG_DATA, "putData: $key: $data")
    }
}






