package com.e.theaauto.kotlin

import android.util.Log
import com.e.theaauto.network.NetworkService
import com.e.theaauto.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private val client = OkHttpClient.Builder()
        .addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .method(original.method(), original.body())
                    .build()
            val okHeaders = request.headers()
            Log.d(Constants.TAG_API, "provideOkHttp: $okHeaders")
            return@addInterceptor chain.proceed(request)
        }.build()
private val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(Constants.BASE_URL)
        .client(client)
        .build()

val networkService: NetworkService = retrofit.create(NetworkService::class.java)