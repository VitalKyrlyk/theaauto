package com.e.theaauto.kotlin

import android.content.Context
import android.content.SharedPreferences
import com.e.theaauto.utils.Constants

fun Context.sharedPreference(): SharedPreferences {
    return getSharedPreferences(Constants.TAG_LOGIN, Context.MODE_PRIVATE)
}