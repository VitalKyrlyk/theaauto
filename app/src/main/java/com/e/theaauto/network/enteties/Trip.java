package com.e.theaauto.network.enteties;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Trip {

    @SerializedName("imei")
    public long imei;

    @SerializedName("start")
    public String start;

    @SerializedName("end")
    public String end;

    @SerializedName("distance")
    public long distance;

    @SerializedName("drivingTime")
    public long drivingTime;

    @SerializedName("finished")
    public boolean finished;

    @SerializedName("points")
    public List<Point> points;

    public class Point{
        @SerializedName("time")
        public String time;
        @SerializedName("speed")
        public int speed;
        @SerializedName("geo")
        public List<Float> geo;
    }

}
