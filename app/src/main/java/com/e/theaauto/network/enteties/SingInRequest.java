package com.e.theaauto.network.enteties;

import com.google.gson.annotations.SerializedName;

public class SingInRequest {

    @SerializedName("username")
    private
    String username;
    @SerializedName("password")
    private
    String password;

    public SingInRequest(String username, String password){
        this.username = username;
        this.password = password;
    }

}
