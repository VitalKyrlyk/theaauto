package com.e.theaauto.network.enteties;

import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("id")
    private
    String id;

    @SerializedName("parent")
    private
    String parent;

    @SerializedName("company")
    private
    String company;

    @SerializedName("name")
    private
    String name;

    @SerializedName("accessLevel")
    private
    String accessLevel;

    @SerializedName("email")
    private
    String email;

    @SerializedName("settings")
    private
    SettingsProfile settings;

    @SerializedName("mobileNumber")
    private
    String mobileNumber;

    @SerializedName("telegramUsername")
    private
    String telegramUsername;

    @SerializedName("userRole")
    private
    String userRole;

    public String getId() {
        return id;
    }

    public String getParent() {
        return parent;
    }

    public String getCompany() {
        return company;
    }

    public String getName() {
        return name;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public String getEmail() {
        return email;
    }

    public SettingsProfile getSettings() {
        return settings;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getTelegramUsername() {
        return telegramUsername;
    }

    public String getUserRole() {
        return userRole;
    }

    public class SettingsProfile {

        @SerializedName("overviewMapPosition")
        private
        String overviewMapPosition;

        @SerializedName("timezone")
        private
        String timezone;

        @SerializedName("notificationSettings")
        private
        String notificationSettings;

        public String getOverviewMapPosition() {
            return overviewMapPosition;
        }

        public String getTimezone() {
            return timezone;
        }

        public String getNotificationSettings() {
            return notificationSettings;
        }
    }
}
