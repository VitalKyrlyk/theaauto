package com.e.theaauto.network.enteties;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Car {

    @SerializedName("id")
    public String id;

    @SerializedName("imei")
    public String imei;

    @SerializedName("company")
    public String company;

    @SerializedName("dealerId")
    public String dealerId;

    @SerializedName("status")
    public String status;

    @SerializedName("nickname")
    public String nickname;

    @SerializedName("vin")
    public String vin;

    @SerializedName("model")
    public String model;

    @SerializedName("manufacturer")
    public String manufacturer;

    @SerializedName("settings")
    public Settings settings;
    private class Settings{
        @SerializedName("general")
        public General general;
        private class General{
            @SerializedName("model")
            public CarSetting model;
            @SerializedName("vin")
            public CarSetting vin;
            @SerializedName("year")
            public CarSetting year;
            @SerializedName("fuel")
            public CarSetting fuel;
            @SerializedName("ignition")
            public CarSetting ignition;
            @SerializedName("vehicle")
            public CarSetting vehicle;
            @SerializedName("maxSpeed")
            public CarSetting maxSpeed;
            @SerializedName("odometer")
            public CarSetting odometer;
            @SerializedName("remoteControlEnabled")
            public boolean remoteControlEnabled;
            @SerializedName("fuelTankCapacity")
            public CarSetting fuelTankCapacity;
            @SerializedName("fuelConsumptionIdle")
            public CarSetting fuelConsumptionIdle;
            @SerializedName("fuelConsumptionCityMode")
            public CarSetting fuelConsumptionCityMode;
            @SerializedName("fuelConsumptionOutOfCity")
            public CarSetting fuelConsumptionOutOfCity;
            @SerializedName("fuelConsumptionWorkingMode")
            public CarSetting fuelConsumptionWorkingMode;
        }
        @SerializedName("engine")
        public Engine engine;
        private  class Engine{
            @SerializedName("maxRoadTime")
            public CarSetting maxRoadTime;
            @SerializedName("revolutionsLimit")
            public CarSetting revolutionsLimit;
            @SerializedName("revolutionsIdle")
            public CarSetting revolutionsIdle;
            @SerializedName("tempLowerBound")
            public CarSetting tempLowerBound;
            @SerializedName("tempUpperBound")
            public CarSetting tempUpperBound;
            @SerializedName("throttlePositionLimit")
            public CarSetting throttlePositionLimit;
            @SerializedName("engineLoadLimit")
            public CarSetting engineLoadLimit;
        }
        @SerializedName("tracker")
        public Tracker tracker;
        public class Tracker{
            @SerializedName("name")
            public CarSetting name;
            @SerializedName("type")
            public CarSetting type;
            @SerializedName("sim1Number")
            public CarSetting sim1Number;
            @SerializedName("sim2Number")
            public CarSetting sim2Number;
            @SerializedName("sim1Operator")
            public CarSetting sim1Operator;
            @SerializedName("sim2Operator")
            public  CarSetting sim2Operator;
            @SerializedName("sim1Roaming")
            public CarSetting sim1Roaming;
            @SerializedName("sim2Roaming")
            public CarSetting sim2Roaming;
            @SerializedName("hasIgnition")
            public boolean hasIgnition;
            @SerializedName("remoteSettings")
            public RemoteSettings remoteSettings;
            public class RemoteSettings{
                @SerializedName("enabled")
                public boolean enabled;
                @SerializedName("value")
                public Value value;
                public class Value{
                    @SerializedName("engineStartTemp")
                    public  CarSetting engineStartTemp;
                    @SerializedName("engineStopTemp")
                    public CarSetting engineStopTemp;
                    @SerializedName("startEngineBatteryVolt")
                    public CarSetting startEngineBatteryVolt;
                    @SerializedName("engineStopAfterMinutes")
                    public CarSetting engineStopAfterMinutes;
                    @SerializedName("engineStartBlockFuelLevel")
                    public CarSetting engineStartBlockFuelLevel;
                }
            }
            @SerializedName("sensitivitySensorSettings")
            public SensitivitySensorSettings sensitivitySensorSettings;
            public class SensitivitySensorSettings{
                @SerializedName("enabled")
                public  boolean enabled;
                @SerializedName("value")
                Value value;
                public class Value{
                    @SerializedName("primarySensorWarningLevel")
                    public CarSetting primarySensorWarningLevel;
                    @SerializedName("primarySensorAlertLevel")
                    public CarSetting primarySensorAlertLevel;
                    @SerializedName("movementSensorLevel")
                    public CarSetting movementSensorLevel;
                    @SerializedName("secondarySensorWarningLevel")
                    public CarSetting secondarySensorWarningLevel;
                    @SerializedName("secondarySensorAlertLevel")
                    public CarSetting secondarySensorAlertLevel;
                    @SerializedName("tiltSensorLevel")
                    public CarSetting tiltSensorLevel;
                }
            }
        }
        @SerializedName("fuelCalibration")
        public FuelCalibration fuelCalibration;
        public class FuelCalibration{
            @SerializedName("values")
            public List values;
        }
        @SerializedName("maintenanceSettings")
        public MaintenanceSettings maintenanceSettings;
        public class MaintenanceSettings{
            @SerializedName("maintenancesByMileage")
            public List<Maintenance> maintenancesByMileage;
            public class Maintenance{
                @SerializedName("enabled")
                public boolean enabled;
                @SerializedName("changedDate")
                public String changedDate;
                @SerializedName("mileageAfterChange")
                public int mileageAfterChange;
                @SerializedName("changeDistance")
                public int changeDistance;
                @SerializedName("maintenanceType")
                public String maintenanceType;
            }
        }
        @SerializedName("remoteControlEnabled")
        public boolean remoteControlEnabled;
    }

    public class CarSetting{

        @SerializedName("enabled")
        public boolean enabled;

        @SerializedName("value")
        public String value;

    }

}
