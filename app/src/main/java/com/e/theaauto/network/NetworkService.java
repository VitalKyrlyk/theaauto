package com.e.theaauto.network;

import com.e.theaauto.network.enteties.Car;
import com.e.theaauto.network.enteties.CarState;
import com.e.theaauto.network.enteties.Profile;
import com.e.theaauto.network.enteties.SingInRequest;
import com.e.theaauto.network.enteties.SingInResponce;
import com.e.theaauto.network.enteties.Trip;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface NetworkService {

    @POST("/auth/login")
    Observable<SingInResponce> singIn(@Body SingInRequest singInRequest);

    @POST("/auth/logout")
    Observable<ResponseBody> logout(@Header("Authorization") String token);

    @GET("/profile")
    Observable<Profile> getProfile(@Header("Authorization") String token);

    @GET("/cars")
    Observable<List<Car>> getCars(@Header("Authorization") String token);

    @GET("/cars/states")
    Observable<List<CarState>> getCarsStates(@Header("Authorization") String token);

    @GET("/trips/car/123456789099221/period/2019-09-23/2019-09-24/0300/details")
    Observable<List<Trip>> getTrips(@Header("Authorization") String token);

}
