package com.e.theaauto.network.enteties;

import com.google.gson.annotations.SerializedName;

public class CarState {

    @SerializedName("imei")
    public String imei;

    @SerializedName("state")
    public String state;

    @SerializedName("speed")
    public int speed;

    @SerializedName("altitude")
    public int altitude;

    @SerializedName("latitude")
    public float latitude;

    @SerializedName("longitude")
    public float longitude;

    @SerializedName("temperatureInside")
    public int temperatureInside;

    @SerializedName("temperatureOutside")
    public int temperatureOutside;

    @SerializedName("fuelLevelLiters")
    public int fuelLevelLiters;

    @SerializedName("fuelLevelPercentage")
    public int fuelLevelPercentage;

}
